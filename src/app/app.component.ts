import { ToastServiceProvider } from './../providers/toast-service';
import { Component, NgZone, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Nav, Platform, ModalController, Events } from 'ionic-angular';

import { AppRate } from '@ionic-native/app-rate';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

import { ContactPage } from '../pages/side-menu/contact/contact';
import { IntroPage } from '../pages/intro/intro';
import { SettingsPage } from '../pages/side-menu/settings/settings';
import { TabsPage } from '../pages/tabs/tabs';

import { DataServiceProvider } from './../providers/data-service';
import { HistoryServiceProvider } from './../providers/history-service';

// User management 

import { AuthServiceProvider } from './../providers/auth-service';
import { LoginPage } from '../pages/auth/login/login';
import { ProfilePage } from '../pages/side-menu/profile/profile';

// Recognition 
import { RecognitionPage } from '../pages/scan/recognition/recognition';

// Utils
import { Logger } from './../providers/utils/logger';

import { timer } from 'rxjs/observable/timer';

import { SingletonServiceProvider } from './../providers/singleton-service';
import { InitializerServiceProvider } from '../providers/initializer-service';
import { Helper } from '../providers/utils/helper';
import { MediaPage } from '../pages/scan/media/media';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TabsPage;

  pages: Array<{ title: Observable<string>, component: any, icon: string }>;

  appInitialized = false;
  appVersionNumber = '2.0.0';
  currentLang = 'en';
  isNetworkEnabled = false;
  networkType = Observable.of('unknown');
  items = [];

  constructor(
    private events: Events,
    private appRate: AppRate,
    private appVersion: AppVersion,
    private platform: Platform,
    private modalCtrl: ModalController,
    private network: Network,
    private openNativeSettings: OpenNativeSettings,
    private ref: ChangeDetectorRef,
    private statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private zone: NgZone,
    private device: Device,
    private translateService: TranslateService,
    private dataService: DataServiceProvider,
    private historyService: HistoryServiceProvider,
    private logger: Logger,
    private authService: AuthServiceProvider,
    public singletonData: SingletonServiceProvider,
    private toastService: ToastServiceProvider,
    private initializer: InitializerServiceProvider,
    private helper: Helper,
  ) {

    this.initSideMenuPages();

    this.platform.ready().then(() => {


      this.events.subscribe('recognition:updated', (doc) => {
        console.log('App');
        const duration = this.helper.getRandomInt(1000, 2000);

        // this.helper.showLoading('Processing recognized image...', duration)
        //   .then(() => {
        let url = doc.title.split(' ')[0];
        // if (url.indexOf('id=') >= 0) {
        //   let id = url.split('id=')[1];
        //   //this.youtube.openVideo(id);
        //   console.log(id);
        //   return;
        // }
        console.log(url);
        //this.nav.setRoot(MediaPage, { url });
        // })
        // .catch(err => {
        //   this.logger.logError(err);
        // });
      });
      // You are on a device, cordova plugins are accessible
      if (this.platform.is('cordova')) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();

        this.statusBar.overlaysWebView(false)
        this.statusBar.backgroundColorByName('black');
      }

      this.initializeApp();
    });
  }

  ngOnInit() {
    this.initializer.initializeNotificationSubscriber();

  }

  // ionViewDidLoad() {
  //   //  The logic from the constructor should come here while testing
  //   this.platform.ready().then(() => {
  //     // You are on a device, cordova plugins are accessible
  //     if (this.platform.is('cordova')) {
  //       this.statusBar.styleDefault();
  //       this.splashScreen.hide();

  //       this.statusBar.overlaysWebView(false)
  //       this.statusBar.backgroundColorByName('black');
  //     }

  //     this.initializeApp();
  //   });
  // }

  initializeApp() {
    this.toastService.hideToast();

    if (this.platform.is('cordova')) {




    }
    this.initializeProperties()
      .then(() => {
        return this.showIntro();
      })
      .then(() => {
        if (this.platform.is('cordova')) {
          this.initAppRate();
        }
      });

    let currentPage = this.nav.getActive();
    if (currentPage.component != RecognitionPage) {

      this.singletonData.showLoading = true;
      this.singletonData.loadingText = "Initializing the mobile application. This shouldn`t take long";
      timer(3000).subscribe(() => this.singletonData.showLoading = false) // <-- hide animation after 3s
    }
  }

  initAppRate() {
    this.appRate.preferences.storeAppURL = {
      android: 'market://details?id=com.app.adar.group',
      ios: '1376090826'
    };

    this.appRate.promptForRating(false);
  }

  initAppVersion(): Promise<void> {
    return this.appVersion.getVersionNumber()
      .then(versionNumber => {
        this.appVersionNumber = versionNumber;
      });
  }


  initNetworkType() {
    if (this.network.type !== 'none')
      this.isNetworkEnabled = true;

    this.networkType = this.translateService.stream('SIDE_MENU.connections.' + this.network.type);

    this.network.onchange()
      .subscribe(() => {
        if (this.network.type !== 'none') {
          this.isNetworkEnabled = true;
        }
        else
          this.isNetworkEnabled = false;

        this.networkType = this.translateService.stream('SIDE_MENU.connections.' + this.network.type);
        this.ref.detectChanges();
      });
  }

  initSideMenuPages() {
    this.pages = [
      {
        title: this.translateService.stream('PAGES.Settings.name'),
        component: SettingsPage,
        icon: 'settings'
      },
      {
        title: this.translateService.stream('PAGES.Contact.name'),
        component: ContactPage,
        icon: 'at'
      }
    ];
  }

  openLogin() {
    this.nav.setRoot(LoginPage);
  }

  openProfile() {
    this.nav.setRoot(ProfilePage);
  }

  initTranslation(): Promise<void> {
    // Update lang property in db on language change
    this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      if (this.platform.is('cordova')) {
        this.appRate.preferences.useLanguage = event.lang;
      }
      this.dataService.setProperty('lang', event.lang);
    });

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    return this.dataService.getProperty('lang')
      .then(lang => {
        this.translateService.use(lang);
      });
  }

  initializeDB(device_id: string): Promise<void> {
    this.dataService.initDB();
    this.dataService.getAll()
      .then(data => {
        this.zone.run(() => {
          this.items = data;
        });
      })
      .catch(err => {
        this.logger.logError(err);
      });

    return this.dataService.setProperty('device_id', device_id)
      .then(() => {
        return this.dataService.getProperty('appInitialized');
      })
      .then(appInitialized => {
        this.appInitialized = appInitialized;

        if (!appInitialized) {
          this.historyService.initializeHistory()
            .then(() => this.dataService.setProperty('appInitialized', true))
            .then(() => this.dataService.setProperty('lang', 'en'));
        }
      });
  }

  initializeProperties(): Promise<void[]> {
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translateService.setDefaultLang('en');

    let device_id = this.device.uuid;
    return this.initializeDB(device_id)
      .then(() => {

        let init = [];

        if (this.platform.is('cordova')) {

          init = [this.initNetworkType(),
          this.initAppVersion(),
          this.initializer.firebaseSetup(),
          this.initTranslation(),
          this.initAuth(),]
        }
        else {

          init = [this.initTranslation(),
          this.initAuth()]
        }

        this.initializer.prefillProperties();

        return Promise.all(init);
      });
  }

  openNetworkSettings() {
    this.openNativeSettings.open('settings');
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  showIntro(): Promise<void> {
    return new Promise((resolve, reject) => {

      let modal = this.modalCtrl.create(IntroPage);
      modal.onDidDismiss(() => {
        resolve();
      });

      if (!this.appInitialized) {
        modal.present();
      } else {
        this.dataService.getProperty('showIntro')
          .then(showIntro => {
            if (showIntro)
              modal.present();
            else
              resolve();
          });
      }
    });
  }

  initAuth(): void {

    let currentPage = this.nav.getActive();
    if (currentPage.component != RecognitionPage) {
      this.authService.getState()
        .subscribe(user => {

          this.authService.bootstrapUser(user);
          this.nav.setRoot(TabsPage);
        }, error => {
          this.rootPage = TabsPage;
        });
    }
  }
}
