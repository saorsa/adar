import { ComponentsModule } from './../components/components.module';
import { MyApp } from './app.component';

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler, DeepLinkConfig } from 'ionic-angular';


import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

//Custom Modules
import { FirebaseModule } from './modules/firebase.module';
import { ServicesModule } from './modules/services.module';
import { PluginsModule } from './modules/plugins.module';
import { PagesModule } from './modules/pages.module';

//Firebase Modules
import { AngularFireModule } from 'angularfire2';
import { FIREBASE_CONFIG } from './app.firebase.config';

//Pages
import { RecognitionPage } from '../pages/scan/recognition/recognition';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export const deepLinkConfig: DeepLinkConfig = {
  links: [
    { component: RecognitionPage, name: "recognition", segment: "media/recognition" }

  ]
};

export class CustomErrorHandler extends IonicErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    super.handleError(err);
    fabric.Crashlytics.addLog("Error: " + err);

    fabric.Answers.sendCustomEvent("code_error", err);
    FirebasePlugin.logEvent("code_error", { error: err });
  }
}

@NgModule({
  declarations: [
    MyApp,
    RecognitionPage,
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true, scrollAssist: false, autoFocusAssist: false }, deepLinkConfig),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    PluginsModule,
    FirebaseModule,
    ServicesModule,
    PagesModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RecognitionPage
  ],

  providers: [
    { provide: ErrorHandler, useClass: CustomErrorHandler }
  ]
})
export class AppModule { }
