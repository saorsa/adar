import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { AboutPage } from '../../pages/tabs/about/about';
import { ContactPage } from '../../pages/side-menu/contact/contact';
import { HistoryPage } from '../../pages/tabs/history/history';
import { HomePage } from '../../pages/tabs/home/home';
import { MediaPage } from '../../pages/scan/media/media';
import { IntroPage } from '../../pages/intro/intro';
import { SettingsPage } from '../../pages/side-menu/settings/settings';
import { TabsPage } from '../../pages/tabs/tabs';
import { LoginPage } from '../../pages/auth/login/login';
import { RegistrationPage } from '../../pages/auth/registration/registration';
import { ProfilePage } from '../../pages/side-menu/profile/profile';
import { DevelopmentPage } from './../../pages/tabs/development/development';

import { SafePipe } from '../../pipes/safe/safe';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { HttpClientModule, HttpClient } from '@angular/common/http';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AboutPage,
    ContactPage,
    HistoryPage,
    HomePage,
    MediaPage,
    IntroPage,
    SettingsPage,
    TabsPage,
    RegistrationPage,
    LoginPage,
    ProfilePage,
    DevelopmentPage,
    SafePipe

  ],
  imports: [
    IonicPageModule.forChild(RegistrationPage),
    IonicPageModule.forChild(LoginPage),
    IonicPageModule.forChild(AboutPage),
    IonicPageModule.forChild(ContactPage),
    IonicPageModule.forChild(HistoryPage),
    IonicPageModule.forChild(HomePage),
    IonicPageModule.forChild(MediaPage),
    IonicPageModule.forChild(IntroPage),
    IonicPageModule.forChild(SettingsPage),
    IonicPageModule.forChild(TabsPage),
    IonicPageModule.forChild(ProfilePage),
    IonicPageModule.forChild(DevelopmentPage),

    ComponentsModule,
    HttpClientModule,

    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ]
})
export class PagesModule { }
