import { NgModule } from '@angular/core';

import { RecognitionProvider } from '../../providers/recognition-service';
import { DataServiceProvider } from '../../providers/data-service';
import { HttpServiceProvider } from '../../providers/http-service';
import { HistoryServiceProvider } from '../../providers/history-service';
import { ContactServiceProvider } from '../../providers/contact-service';
import { SingletonServiceProvider } from '../../providers/singleton-service';
import { InitializerServiceProvider } from '../../providers/initializer-service';
import { ToastServiceProvider } from '../../providers/toast-service';
import { Helper } from '../../providers/utils/helper';
import { Logger } from '../../providers/utils/logger';
import { DataModel } from '../../models/data';

@NgModule({
    providers: [
        DataServiceProvider,
        HistoryServiceProvider,
        RecognitionProvider,
        HttpServiceProvider,
        ContactServiceProvider,
        SingletonServiceProvider,
        InitializerServiceProvider,
        ToastServiceProvider,
        DataModel,
        Helper,
        Logger
    ]
})
export class ServicesModule {

}