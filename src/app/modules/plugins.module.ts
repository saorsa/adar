import { NgModule } from '@angular/core';

import { AppRate } from '@ionic-native/app-rate';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Market } from '@ionic-native/market';
import { Network } from '@ionic-native/network';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Facebook } from '@ionic-native/facebook';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Insomnia } from '@ionic-native/insomnia';
import { Firebase } from '@ionic-native/firebase';

@NgModule({
    providers: [
        AppRate,
        AppVersion,
        Device,
        Market,
        Network,
        OpenNativeSettings,
        StatusBar,
        SplashScreen,
        ScreenOrientation,
        Facebook,
        AndroidPermissions,
        Insomnia,
        Firebase,
    ]
})
export class PluginsModule {

}