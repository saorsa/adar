import { NgModule } from '@angular/core';

import { AuthServiceProvider } from '../../providers/auth-service';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';

@NgModule({
    imports: [
        AngularFireAuthModule,
        AngularFireDatabaseModule
    ],
    providers: [
        AuthServiceProvider,
        AngularFirestore]
})
export class FirebaseModule {
    constructor(afs: AngularFirestore) {
        // Suppress warning from Cloud Firestore
        const settings = { timestampsInSnapshots: true };
        afs.app.firestore().settings(settings)
    }
}