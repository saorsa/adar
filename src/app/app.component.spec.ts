import { async, TestBed } from '@angular/core/testing';
import { IonicModule, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { StatusBarMock } from '@ionic-native-mocks/status-bar';

import { SplashScreen } from '@ionic-native/splash-screen';
import { SplashScreenMock } from '@ionic-native-mocks/splash-screen';

import { AppRate } from '@ionic-native/app-rate';
import { AppRateMock } from '@ionic-native-mocks/app-rate';

import { AppVersion } from '@ionic-native/app-version';
import { AppVersionMock } from '@ionic-native-mocks/app-version';

import { Device } from '@ionic-native/device';
import { DeviceMock } from '@ionic-native-mocks/device';

import { Network } from '@ionic-native/network';
import { NetworkMock } from '@ionic-native-mocks/network';

import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { OpenNativeSettingsMock } from '@ionic-native-mocks/open-native-settings';

import { Firebase } from '@ionic-native/firebase';
import { FirebaseMock } from '@ionic-native-mocks/firebase';

import { Facebook } from '@ionic-native/facebook';
import { FacebookMock } from '@ionic-native-mocks/facebook';

import { AndroidPermissions } from '@ionic-native/android-permissions';
import { AndroidPermissionsMock } from '@ionic-native-mocks/android-permissions';

import { Insomnia } from '@ionic-native/insomnia';
import { InsomniaMock } from '@ionic-native-mocks/insomnia';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { MyApp } from './app.component';
import { PlatformMock } from '../../test-config/mocks-ionic';

//Custom Modules
import { FirebaseModule } from './modules/firebase.module';
import { ServicesModule } from './modules/services.module';
import { PagesModule } from './modules/pages.module';

//Firebase Modules
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FIREBASE_CONFIG } from './app.firebase.config';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('MyApp Component', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyApp],
      imports: [
        IonicModule.forRoot(MyApp),
        AngularFireModule.initializeApp(FIREBASE_CONFIG),
        HttpClientModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [HttpClient]
          }
        }),
        FirebaseModule,
        ServicesModule,
        PagesModule,
      ],
      providers: [
        { provide: StatusBar, useClass: StatusBarMock },
        { provide: SplashScreen, useClass: SplashScreenMock },
        { provide: AppRate, useClass: AppRateMock },
        { provide: AppVersion, useClass: AppVersionMock },
        { provide: Device, useClass: DeviceMock },
        { provide: Network, useClass: NetworkMock },
        { provide: AndroidPermissions, useClass: AndroidPermissionsMock },
        { provide: Insomnia, useClass: InsomniaMock },
        { provide: Firebase, useClass: FirebaseMock },
        { provide: Facebook, useClass: FacebookMock },
        { provide: OpenNativeSettings, useClass: OpenNativeSettingsMock },
        { provide: Platform, useClass: PlatformMock }
      ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof MyApp).toBe(true);
  });

  it('should have two pages', () => {
    expect(component.pages.length).toBe(2);
  });

});
