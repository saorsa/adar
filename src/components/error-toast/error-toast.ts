import { ToastServiceProvider } from './../../providers/toast-service';
import { Component } from '@angular/core';
import { SingletonServiceProvider } from '../../providers/singleton-service';

@Component({
  selector: 'error-toast',
  templateUrl: 'error-toast.html'
})
export class ErrorToastComponent {

  constructor(
    private toastService: ToastServiceProvider,
    public singletonData: SingletonServiceProvider
  ) {
  }

  hideToast() {
    this.toastService.hideToast();
  }
}
