import { SingletonServiceProvider } from './../../providers/singleton-service';
import { ToastServiceProvider } from './../../providers/toast-service';
import { Component } from '@angular/core';


@Component({
  selector: 'success-toast',
  templateUrl: 'success-toast.html'
})
export class SuccessToastComponent {


  constructor(
    private toastService: ToastServiceProvider,
    public singletonData: SingletonServiceProvider
  ) {
  }
  hideToast() {
    this.toastService.hideToast();
  }
}
