import { IonicPageModule } from 'ionic-angular';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ErrorToastComponent } from './error-toast/error-toast';
import { SuccessToastComponent } from './success-toast/success-toast';

@NgModule({
	declarations: [
		ErrorToastComponent,
		SuccessToastComponent
	],
	imports: [
		IonicPageModule.forChild(ErrorToastComponent),
		IonicPageModule.forChild(SuccessToastComponent)
	],
	exports: [
		ErrorToastComponent,
		SuccessToastComponent
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class ComponentsModule { }
