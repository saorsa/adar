export interface Feedback {
    key?: string;
    name: string;
    email: string;
    question: string;
}