export class Item {
  contents: object[];
  custom: string;
  isAR: false;
  name: string;
  rotation: number[];
  translation: number[];
  url: string;
  uuid: string;
}
