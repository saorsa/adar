export class CustomUser {
    uid?: string;
    displayName?: string;
    email?: string;
    photoURL?: string;
    showIntro?: boolean;
    language?: string;
    providerData?: any;
    firebaseToken?: string;
    isAnonymous?: boolean;
    updatesNotif?: boolean;
    newsNotif?: boolean;
    advertsNotif?: boolean;
}
