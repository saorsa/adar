import { CustomUser } from './customuser.model';
import { Observable } from "rxjs/Observable";
import { Observer } from "rxjs/Observer";
import "rxjs/add/operator/share";

export class DataModel {

  private _device_id: string;
  private _lang: string;
  private _user: CustomUser;
  private _firebaseToken: string;
  private _showIntro: boolean;


  updatesNotif: Observable<boolean>;
  private updatesNotifОbserver: Observer<boolean>;

  newsNotif: Observable<boolean>;
  private newsNotifОbserver: Observer<boolean>;

  advertsNotif: Observable<boolean>;
  private advertsNotifОbserver: Observer<boolean>;

  constructor() {
    this.updatesNotif = new Observable<boolean>(observer =>
      this.updatesNotifОbserver = observer
    ).share();

    this.newsNotif = new Observable<boolean>(observer =>
      this.newsNotifОbserver = observer
    ).share();

    this.advertsNotif = new Observable<boolean>(observer =>
      this.advertsNotifОbserver = observer
    ).share();
  }

  changeUpdatesNotif(newStatus: boolean) {
    if (this.updatesNotifОbserver !== undefined) this.updatesNotifОbserver.next(newStatus);
  }
  changeNewsNotif(newStatus: boolean) {
    if (this.newsNotifОbserver !== undefined) this.newsNotifОbserver.next(newStatus);
  }
  changeAdvertsNotif(newStatus: boolean) {
    if (this.advertsNotifОbserver !== undefined) this.advertsNotifОbserver.next(newStatus);
  }

  public get device_id(): string {
    return this._device_id;
  }

  public set device_id(value: string) {
    this._device_id = value;
  }

  public get showIntro(): boolean {
    return this._showIntro;
  }

  public set showIntro(value: boolean) {
    this._showIntro = value;
  }


  public get lang(): string {
    return this._lang;
  }

  public set lang(value: string) {
    this._lang = value;
  }
  public get user(): CustomUser {
    return this._user;
  }

  public set user(value: CustomUser) {
    this._user = value;
  }
  public get firebaseToken(): string {
    return this._firebaseToken;
  }

  public set firebaseToken(value: string) {
    this._firebaseToken = value;
  }

}
