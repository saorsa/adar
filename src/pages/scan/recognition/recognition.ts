import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';

import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { RecognitionProvider } from '../../../providers/recognition-service';

import { Logger } from '../../../providers/utils/logger';

import { Insomnia } from '@ionic-native/insomnia';

@Component({
  selector: 'page-recognition',
  templateUrl: 'recognition.html',
})
export class RecognitionPage {

  public showSplash: Boolean = true;
  public splashText: String;


  constructor(
    private platform: Platform,
    private recognition: RecognitionProvider,
    private screenOrientation: ScreenOrientation,
    private logger: Logger,
    private insomnia: Insomnia
  ) {
    this.platform.ready()
      .then(() => {

        FirebasePlugin.setScreenName("Recognition");
        fabric.Answers.sendContentView("scan", "screen", "recognition-screen");

        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);


        this.initSettingsRecogniction();

      })
      .catch(error => {
        this.logger.logError(error);
      });

  }

  closeRecognition(): void {
    this.insomnia.allowSleepAgain()
      .then(
        () => console.log('success'),
        () => console.log('error')
      );

    this.recognition.stopCraftAR();

  }

  goBack(): void {

    this.closeRecognition();
  }

  private initSettingsRecogniction(): void {


    //timer(3000).subscribe(() => this.showSplash = false) // <-- hide animation after 3s

    this.insomnia.keepAwake()
      .then(
        () => console.log('success'),
        () => console.log('error')
      );

    return this.recognition.startCraftAR();
  }
}
