import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

import { Logger } from "../../../providers/utils/logger";

@Component({
  selector: 'page-media',
  templateUrl: 'media.html',
})
export class MediaPage {

  mediaUrl: string = "assets/img/white.jpg";

  constructor(
    private platform: Platform,
    private navCtrl: NavController,
    private navParams: NavParams,
    private logger: Logger
  ) {
    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Media playback");
        fabric.Answers.sendContentView("scan","screen", "media-playback-screen");
        let url = this.navParams.data.url;

        if (url.indexOf('/forms/') > 0) {
          this.mediaUrl = url;
        } else {
          this.mediaUrl = this.addAutoPlayOption(<string>url);
        }
      })
      .catch(error => {
        this.logger.logError(error);
      });
  }

  closePlayback(): void {
    this.navCtrl.pop();
  }

  private addAutoPlayOption(url: string): string {
    let result = url;
    if (url) {
      if (url.indexOf("?") === -1)
        result += "?autoplay=1";
      else
        result += "&autoplay=1";
    }
    return result;
  }

}
