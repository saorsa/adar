import { TabsPage } from './../../tabs/tabs';
import { ToastServiceProvider } from './../../../providers/toast-service';
import { TranslateService } from '@ngx-translate/core';
import { AuthServiceProvider } from './../../../providers/auth-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { Helper } from '../../../providers/utils/helper';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  public registartionForm: FormGroup;

  constructor(
    private nav: Nav,
    private formBuilder: FormBuilder,
    private authService: AuthServiceProvider,
    private toastService: ToastServiceProvider,
    private helper: Helper,
    private translateService: TranslateService,
    private platform: Platform
  ) {

    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Registration");
        fabric.Answers.sendContentView("auth", "screen", "registration-screen");
      });

    this.registartionForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }


  loginRedirect() {
    this.nav.setRoot(LoginPage);
  }

  singin(provider: string) {

    if (this.helper.doesNotHaveInternetConnection()) {
      this.translateService.get('ERRORS.noInternetConnectionScan').subscribe((res: string) => {
        this.toastService.showError(res);
      });
      return;
    }

    this.authService.signIn(provider);
  }


  signUp() {

    if (this.helper.doesNotHaveInternetConnection()) {
      this.translateService.get('ERRORS.noInternetConnectionScan').subscribe((res: string) => {
        this.toastService.showError(res);
      });
      return;
    }

    let user = this.registartionForm.value;
    this.registartionForm.reset();

    this.authService.signUp(user)

  }

  onBackButtonClicked() {
    this.nav.setRoot(TabsPage);
  }
  languageChanged(lang: string) {
    this.translateService.use(lang);
  }
}
