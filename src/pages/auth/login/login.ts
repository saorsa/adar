import { Helper } from './../../../providers/utils/helper';
import { ToastServiceProvider } from './../../../providers/toast-service';
import { TranslateService } from '@ngx-translate/core';

import { RegistrationPage } from '../registration/registration';
import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Nav, Platform } from 'ionic-angular';

import { AuthServiceProvider } from '../../../providers/auth-service';
import { TabsPage } from '../../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthServiceProvider,
    private nav: Nav,
    private platform: Platform,
    private translateService: TranslateService,
    private helper: Helper,
    private toastService: ToastServiceProvider
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Login");
        fabric.Answers.sendContentView("auth", "screen", "login-screen");
      });
  }

  signout() {
    this.authService.logout();
  }

  public singin(provider: string) {

    if (this.helper.doesNotHaveInternetConnection()) {
      this.translateService.get('ERRORS.noInternetConnectionScan').subscribe((res: string) => {
        this.toastService.showError(res);
      });
      return;
    }

    let user = this.loginForm.value;
    this.loginForm.reset();

    this.authService.signIn(provider, user);
  }

  public register() {
    this.nav.setRoot(RegistrationPage);
  }
  public onBackButtonClicked() {
    this.nav.setRoot(TabsPage);
  }
  public languageChanged(lang: string) {
    this.translateService.use(lang);
  }

}
