import { Component, ViewChild } from '@angular/core';
import {  ViewController, Slides, Platform } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { DataServiceProvider } from '../../providers/data-service';

@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  @ViewChild(Slides) slides: Slides;

  walktroughts: any[] = [];

  constructor(
    private viewCtrl: ViewController,
    private translateService: TranslateService,
    private dataService: DataServiceProvider,
    private platform: Platform
  ) {
    this.platform.ready()
      .then(() => {
        if (this.platform.is('cordova')) {
          FirebasePlugin.setScreenName("Intro");
          fabric.Answers.sendContentView("tabs-menu", "screen", "intro-screen");
        }
      });
  }

  public goToHome() {
    this.viewCtrl.dismiss();
  }

  public skip() {
    this.dataService.addOrUpdate('showIntro', false)
      .then(() => {
        this.viewCtrl.dismiss();
      })
      .catch(() => {
        this.viewCtrl.dismiss();
      });
  }

  public nextSlide() {
    if (this.slides.isEnd()) {
      this.goToHome();
      return;
    }

    this.slides.slideNext(1000)
  }

  ionViewWillEnter() {
    this.translateService.get('PAGES.Intro.slides')
      .subscribe(slides => {
        this.walktroughts = slides;
      });
  }

}
