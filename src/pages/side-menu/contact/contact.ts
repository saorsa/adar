import { Logger } from './../../../providers/utils/logger';
import { CustomUser } from './../../../models/customuser.model';
import { DataModel } from './../../../models/data';
import { ToastServiceProvider } from './../../../providers/toast-service';
import { Helper } from '../../../providers/utils/helper';
import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Nav, Platform } from 'ionic-angular';

import { TabsPage } from '../../tabs/tabs';

import { ContactServiceProvider } from '../../../providers/contact-service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {
  contactForm: FormGroup;

  customUser: CustomUser = {
    displayName: "",
    email: ""
  };

  constructor(
    private formBuilder: FormBuilder,
    private nav: Nav,
    private contactServiceProvider: ContactServiceProvider,
    private helper: Helper,
    private translateService: TranslateService,
    private toastService: ToastServiceProvider,
    public dataModel: DataModel,
    private platform: Platform,
    private logger: Logger
  ) {

    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Contacts");
        fabric.Answers.sendContentView("side-menu", "screen", "contacts-screen");
      });

    if (dataModel.user) {
      this.customUser = dataModel.user;
    }

    this.contactForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', [Validators.required, Validators.email]],
      question: ['', [Validators.required]]
    });
  }

  logForm() {

    if (this.helper.doesNotHaveInternetConnection()) {
      this.translateService.get('ERRORS.noInternetConnectionScan').subscribe((res: string) => {
        this.toastService.showError(res);
      });
      return;
    }

    this.contactServiceProvider.sendFeedback(this.contactForm.value)
      .then(ref => {
        this.toastService.showSuccess("You have successufully sent your feedback. Thank you :)");
        this.nav.setRoot(TabsPage)
      })
      .catch(err => {
        this.toastService.showError("Something went wrong while sending your feedback. Please try again");
        this.logger.logError(err);

      })
  }

  onBackButtonClicked() {
    this.nav.setRoot(TabsPage);
  }

}
