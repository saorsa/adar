import { CustomUser } from './../../../models/customuser.model';
import { DataModel } from './../../../models/data';
import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { TabsPage } from '../../tabs/tabs';

import { AuthServiceProvider } from '../../../providers/auth-service';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  customUser: CustomUser;

  constructor(
    private nav: Nav,
    private authService: AuthServiceProvider,
    public dataModel: DataModel,
    private platform: Platform
  ) {
    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Profile");
        fabric.Answers.sendContentView("side-menu", "screen", "profile-screen");
      });

    this.customUser = dataModel.user;
  }

  onBackButtonClicked() {
    this.nav.setRoot(TabsPage);
  }
  logout(): void {
    this.authService.logout();
    this.onBackButtonClicked();
  }
}
