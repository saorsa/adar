import { SingletonServiceProvider } from './../../../providers/singleton-service';
import { Helper } from './../../../providers/utils/helper';
import { CustomUser } from './../../../models/customuser.model';
import { AuthServiceProvider } from './../../../providers/auth-service';
import { Component } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';

import { AppRate } from '@ionic-native/app-rate';
import { Market } from '@ionic-native/market';

import { TabsPage } from '../../tabs/tabs';

import { DataServiceProvider } from '../../../providers/data-service';
import { Logger } from '../../../providers/utils/logger';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  showIntro: boolean = true;
  generalNotif: boolean = true;
  updatesNotif: boolean = true;
  newsNotif: boolean = true;
  advertsNotif: boolean = true;
  customUser: CustomUser;
  selectedLanguage: any;

  constructor(
    private appRate: AppRate,
    private market: Market,
    private nav: Nav,
    private dataService: DataServiceProvider,
    private logger: Logger,
    private authService: AuthServiceProvider,
    private translateService: TranslateService,
    private platform: Platform,
    private helper: Helper,
    private singletonData: SingletonServiceProvider
  ) {

    this.platform.ready()
      .then(() => {
        FirebasePlugin.setScreenName("Settings");
        fabric.Answers.sendContentView("side-menu", "screen", "settings-screen");
      });

    this.getPropertiesFromDB();

  }
  //For anonymous users
  private getPropertiesFromDB() {
    this.dataService.getProperty('showIntro')
      .then(showIntro => {
        this.showIntro = showIntro;
        return this.dataService.getProperty('updatesNotif');
      })
      .then(updatesNotif => {
        this.updatesNotif = updatesNotif;
        return this.dataService.getProperty('newsNotif');
      })
      .then(newsNotif => {
        this.newsNotif = newsNotif;
        return this.dataService.getProperty('advertsNotif');
      })
      .then(advertsNotif => {
        this.advertsNotif = advertsNotif;
        return this.dataService.getProperty('lang');
      })
      .then(lang => {
        this.selectedLanguage = lang;
        //   return this.dataService.getProperty('generalNotif');
        // })
        // .then(generalNotif => {
        //   this.generalNotif = generalNotif;

      });
  }

  //Need check for anonymous user - Probably to lock notifications ? 
  onBackButtonClicked() {
    this.nav.setRoot(TabsPage);
  }

  checkForUpdates() {
    // TODO: Check platform
    this.market.open('com.app.adar.group');
  }

  rateApp() {
    this.appRate.promptForRating(true);
  }

  updateShowIntro(checked) {
    this.dataService.addOrUpdate('showIntro', checked)
      .catch(err => {
        this.logger.logError(err);
      });
  }

  updateUpdatesNotif(checked) {
    this.helper.updateUpdatesNotif(checked);
  }

  updateNewsNotif(checked) {
    this.helper.updateNewsNotif(checked);
  }

  updateAdvertsNotif(checked) {
    this.helper.updateAdvertsNotif(checked);
  }
  updateGeneralNotif(checked) {
    this.helper.updateGeneralNotif(checked);
  }

  languageChanged(lang: any) {
    this.translateService.use(lang);
  }

  ionViewWillLeave() {
    if (this.singletonData.loggedIn) {
      this.helper.getUserFromDB()
        .then(userDB => {
          let user = {
            uid: userDB.uid,
            email: userDB.email,
            displayName: userDB.displayName,
            photoURL: userDB.photoURL,
            showIntro: this.showIntro,
            language: this.selectedLanguage,
            providerData: userDB.providerData,
            firebaseToken: userDB.fbToken,
            isAnonymous: userDB.isAnonymous,
            updatesNotif: this.updatesNotif,
            newsNotif: this.newsNotif,
            advertsNotif: this.advertsNotif
          }
          this.authService.updateUser(user)
        })
        .catch(err => {
          this.logger.logError(err);
        });
    }
  }

}
