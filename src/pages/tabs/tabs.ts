// import { DevelopmentPage } from './development/development';
import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { AboutPage } from './about/about';
// import { HistoryPage } from './../history/history';
import { HomePage } from './home/home';
import { IntroPage } from '../intro/intro';
// import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  // tab3Root = HistoryPage;
  // tab4Root = DevelopmentPage;
  // tab5Root = LoginPage;

  constructor(
    private modalCtrl: ModalController
  ) { }

  showIntro() {
    let modal = this.modalCtrl.create(IntroPage, null, { cssClass: "modal-fullscreen" });
    modal.present();
  }
}
