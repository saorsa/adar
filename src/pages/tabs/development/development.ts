import { ToastServiceProvider } from './../../../providers/toast-service';
import { Component } from '@angular/core';
import { IonicPage, Platform } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-development',
  templateUrl: 'development.html',
})
export class DevelopmentPage {

  constructor(
    private toastService: ToastServiceProvider,
    private platform: Platform,
  ) {
    this.platform.ready()
      .then(() => {

        if (this.platform.is('cordova')) {
          FirebasePlugin.setScreenName("Development");
          fabric.Answers.sendContentView("tabs-menu","screen", "development-screen");
        }
      });
  }

  showError() {
    this.toastService.showError("Error");
  }
  showSuccess() {
    this.toastService.showSuccess("Success");
  }
  fatalError() {
    fabric.Crashlytics.addLog("about to send a crash for testing!");
    fabric.Crashlytics.sendCrash();
    this.toastService.showSuccess('Success');
  }
  nonfatalError() {
    //Android and iOS
    fabric.Crashlytics.addLog("about to send a non fatal crash for testing!");
    fabric.Crashlytics.sendNonFatalCrash("Error message");

    this.toastService.showSuccess('Success');
  }
  customLoging() {
    console.log("Custom Loging")
    let attributes = {
      foo: "data",
      bar: "string",
    };

    fabric.Answers.sendSignUp("Facebook", true, attributes);
    this.toastService.showSuccess('Success');
  }

  sendInfo() {
    fabric.Crashlytics.setUserIdentifier("123");
    fabric.Crashlytics.setUserName("Some Guy");
    fabric.Crashlytics.setUserEmail("some.guy@email.com");
    fabric.Crashlytics.setStringValueForKey("bar", "foo");
    this.toastService.showSuccess('Success');
  }


}
