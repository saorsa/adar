import { Component } from '@angular/core';
import { ActionSheetController, NavController, NavParams, Gesture, Platform } from 'ionic-angular';

import { HistoryServiceProvider } from '../../../providers/history-service';
import { MediaPage } from "../../scan/media/media";

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  history: any[];

  constructor(
    public actionSheetController: ActionSheetController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private historyService: HistoryServiceProvider,
    private platform: Platform
  ) {
    this.platform.ready()
      .then(() => {
        if (this.platform.is('cordova')) {
          FirebasePlugin.setScreenName("History");
          fabric.Answers.sendContentView("tabs-menu", "screen", "history-screen");
        }
      });
  }

  ionViewWillEnter() {
    this.historyService.get()
      .then(history => {
        this.history = history.reverse();
      });
  }

  showMedia(url: string) {
    let actionSheet = this.actionSheetController.create({
      title: 'Желаете ли да отворите видео',
      buttons: [
        {
          text: 'Да',
          handler: () => {
            this.navCtrl.push(MediaPage, { url });
          }
        },
        {
          text: 'Не',
          role: 'cancel'
        }
      ]
    });

    actionSheet.present();
  }

  swipeEvent(e: Gesture) {
    if (e.direction == '2')
      this.navCtrl.parent.select(2);
    else if (e.direction == '4')
      this.navCtrl.parent.select(0);
  }
}
