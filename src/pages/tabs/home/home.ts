import { ToastServiceProvider } from './../../../providers/toast-service';
import { Component } from '@angular/core';
import { NavController, Platform, Events, Gesture } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { MediaPage } from '../../scan/media/media';

import { Helper } from '../../../providers/utils/helper';
import { Logger } from '../../../providers/utils/logger';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    private events: Events,
    private navCtrl: NavController,
    private platform: Platform,
    private helper: Helper,
    private logger: Logger,
    private translateService: TranslateService,
    private toastService: ToastServiceProvider
  ) {


    this.platform.ready()
      .then(() => {
        console.log(this.platform.versions());
        console.log(this.platform.is('cordova'))
        // if (this.platform.is('cordova')) {

        // FirebasePlugin.setScreenName("Home");
        // fabric.Answers.sendContentView("tabs-menu", "screen", "home-screen");

        this.events.subscribe('recognition:updated', (doc) => {
          console.log('Home');
          // const duration = this.helper.getRandomInt(1000, 2000);

          // this.helper.showLoading('Processing recognized image...', duration)
          //   .then(() => {
          let url = doc.title.split(' ')[0];
          // if (url.indexOf('id=') >= 0) {
          //   let id = url.split('id=')[1];
          //   //this.youtube.openVideo(id);
          //   console.log(id);
          //   return;
          // }

          this.navCtrl.push(MediaPage, { url });
          // })
          // .catch(err => {
          //   this.logger.logError(err);
        });
        //});

        this.events.subscribe('error:scan', (doc) => {
          this.toastService.showError("Something went wrong while setting up the recognition algorithms. Please try again.");
        });
        // }
      });

  }

  startFinderMode() {
    this.startScanMode();
  }
  private startScanMode(): void {
    if (this.helper.doesNotHaveInternetConnection()) {
      this.translateService.get('ERRORS.noInternetConnectionScan').subscribe((res: string) => {
        this.toastService.showError(res);
      });
      return;
    }

    this.helper.askPermissions()
      .then(result => {
        CraftARSDK.startView(null, null, {
          "loadUrl": "index.html#/home/media/recognition"
        });
      })
      .catch(err => {
        this.logger.logError(err);
      });

  }

  languageChanged(lang: string) {
    this.translateService.use(lang);
  }

  swipeEvent(e: Gesture) {
    if (e.direction == '2')
      this.navCtrl.parent.select(1);
  }
}
