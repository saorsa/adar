import { ContactPage } from './../../side-menu/contact/contact';
import { Component } from '@angular/core';
import { NavController, Gesture, Platform } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private translateService: TranslateService
  ) {

    this.platform.ready()
      .then(() => {
        if (this.platform.is('cordova')) {
          FirebasePlugin.setScreenName("About");
          fabric.Answers.sendContentView("tabs-menu", "screen", "about-screen");
        }
      });
  }

  swipeEvent(e: Gesture) {
    if (e.direction == '4')
      this.navCtrl.parent.select(0)
  }

  languageChanged(lang: string) {
    this.translateService.use(lang);
  }

  public showContacts() {
    this.navCtrl.setRoot(ContactPage);
  }

}
