import { ToastServiceProvider } from './toast-service';
import { Helper } from './utils/helper';
import { SingletonServiceProvider } from './singleton-service';
import { DataServiceProvider } from './data-service';
import { DataModel } from './../models/data';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';


@Injectable()
export class InitializerServiceProvider {

  updatesNotifSubscription: any;
  newsNotifubscription: any;
  advertsNotifSubscription: any;

  constructor(
    private dataModel: DataModel,
    private dataService: DataServiceProvider,
    private singletonData: SingletonServiceProvider,
    private helper: Helper,
    private toastService: ToastServiceProvider,
    private platform: Platform
  ) {
  }

  initializeApp() {
  }

  initializeNotificationSubscriber() {
    this.updatesNotifSubscription = this.dataModel.updatesNotif.subscribe(status => {
      console.log("Updates notification status: " + status);
      if (status) {
        FirebasePlugin.subscribe("Updates");
      }
      else {
        FirebasePlugin.unsubscribe("Updates");
      }

    });
    this.newsNotifubscription = this.dataModel.newsNotif.subscribe(status => {
      console.log("News notification status: " + status);
      if (status) {
        FirebasePlugin.subscribe("Newsletter");
      }
      else {
        FirebasePlugin.unsubscribe("Newsletter");
      }
    });
    this.advertsNotifSubscription = this.dataModel.advertsNotif.subscribe(status => {
      console.log("Advertisements notification status: " + status);
      if (status) {
        FirebasePlugin.subscribe("Advertisements");
      }
      else {
        FirebasePlugin.unsubscribe("Advertisements");
      }
    });
  }

  firebaseSetup() {
    //From settings
    //FirebasePlugin.unsubscribe("example");

    FirebasePlugin.startTrace("test trace", () => {
      console.log("Trace started sucessfully");
    }, () => {
      console.log("Erro: Starting trace");
    });

    FirebasePlugin.hasPermission(data => {
      if (data.isEnabled) {

      }
      else {
        FirebasePlugin.grantPermission();
      }
    });

    FirebasePlugin.getToken(token => {

      //Firebase Topics
      //Plugin documentation - https://www.npmjs.com/package/cordova-plugin-firebase
      FirebasePlugin.subscribe("General");
      FirebasePlugin.subscribe("Warning");
      FirebasePlugin.subscribe("Logging");
      FirebasePlugin.subscribe("Error");

      if (this.platform.is('ios')) {
        FirebasePlugin.subscribe("iPhone");
      }
      else if (this.platform.is('android')) {
        FirebasePlugin.subscribe("Android");
      }

      this.dataService.getProperty('updatesNotif')
        .then(updatesNotif => {
          FirebasePlugin.subscribe("Updates");
          return this.dataService.getProperty('newsNotif');
        })
        .then(newsNotif => {
          FirebasePlugin.subscribe("Newsletter");
          return this.dataService.getProperty('advertsNotif');
        })
        .then(advertsNotif => {
          FirebasePlugin.subscribe("Advertisements");
        });

    }, error => {
      console.error(error);
    });

    FirebasePlugin.onTokenRefresh(token => {
      this.dataService.addOrUpdate('firebaseToken', token);
      this.singletonData.firebaseToken = token;
    }, error => {
      console.error(error);
    });

    FirebasePlugin.onNotificationOpen(notification => {

      if (notification.tap) {
        console.log(notification);
      }
      else {
        this.toastService.showSuccess(notification.body);
      }
    }, error => {
      console.error(error);
    });

    //Remote config

    FirebasePlugin.fetch(600, result => {
      // activate the fetched remote config
      // console.log(JSON.stringify(result)); //Always "OK"
      FirebasePlugin.activateFetched(
        // Android seems to return error always, so we want to cath both
        result => {
          // console.log(JSON.stringify(result)); //either true or false
          FirebasePlugin.getValue("sync_collection", result => {
            this.singletonData.syncCollection = result
          }, reason => {
            console.warn(`Error  ${reason}`);
          });
          FirebasePlugin.getValue("collection_token", result => {
            this.singletonData.collectionToken = result
          }, reason => {
            console.warn(`Error  ${reason}`);
          });
        }, reason => {
          console.warn(reason);
        }
      )
    });
  }

  prefillProperties() {
    this.singletonData.syncCollection = "true";
    this.singletonData.collectionToken = "68d26f46e6154af1";

    this.helper.updateGeneralNotif(true);
    this.helper.updateUpdatesNotif(true);
    this.helper.updateNewsNotif(true);
    this.helper.updateAdvertsNotif(true);
  }
}
