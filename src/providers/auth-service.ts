import { ToastServiceProvider } from './toast-service';
import { DataModel } from './../models/data';
import { Helper } from './utils/helper';
import { DataServiceProvider } from './data-service';
import { User } from '@firebase/auth-types';
import { CustomUser } from '../models/customuser.model';

import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs/Observable';
import { Facebook } from '@ionic-native/facebook';
import { Platform } from 'ionic-angular';

import { SingletonServiceProvider } from '../providers/singleton-service';

import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

// import { Logger } from './utils/logger';


@Injectable()
export class AuthServiceProvider {

  private customUser: Observable<CustomUser>;
  private userData: CustomUser;
  private fbToken: string = "No token";

  constructor(
    public afAuth: AngularFireAuth,
    public fb: Facebook,
    public platform: Platform,
    private singletonData: SingletonServiceProvider,
    private afs: AngularFirestore,
    private dataModel: DataModel,
    private helper: Helper,
    private dataService: DataServiceProvider,
    // private logger: Logger,
    private toastService: ToastServiceProvider
  ) {

    this.customUser = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.afs.doc<CustomUser>(`users/${user.uid}`).valueChanges()
        } else {
          return Observable.of(null)
        }
      })
  }

  public updateUser(user) {
    let self = this;

    self.fbToken = self.singletonData.firebaseToken

    this.userData = self.helper.formatDataFromProvider(user, this.fbToken, null);
    this.setUserFirebase(this.userData);
  }
  public bootstrapUser(user) {
    let self = this;

    self.fbToken = self.singletonData.firebaseToken

    if (user) {
      this.getUserFromFBDB(user)
        .then(userFB => {
          this.userData = self.helper.formatDataFromProvider(user, this.fbToken, userFB);
          this.setUserFirebase(this.userData);
        })
        .catch(err => {
          this.updateUser(user);
        })
    }
    else {
      this.singletonData.loggedIn = false;
    }
    return null;

  }

  private getUserFromFBDB(user): Promise<any> {
    return new Promise((resolve, reject) => {

      const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

      userRef.ref.get().then((doc) => {
        if (doc.exists) {
          return resolve(doc.data())
        } else {
          return reject();
        }
      })
        .catch(err => {
          reject();
        });
    });
  }

  public getState(): Observable<User> {
    if (this.helper.doesNotHaveInternetConnection()) {
      return Observable.of(null);
    }
    return this.afAuth.authState;
  }


  public logout(): Promise<any> {
    this.dataModel.user = null;
    this.helper.updateGeneralNotif(true);
    this.helper.updateUpdatesNotif(true);
    this.helper.updateNewsNotif(true);
    this.helper.updateAdvertsNotif(true);
    return this.afAuth.auth.signOut();
  }

  public signUp(user: any) {

    let email = user.email;
    let password = user.password;
    let name = user.name;

    let loading = this.helper.createLoading('Signing you up...', 'bubbles');

    loading.present();

    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.afAuth.auth.currentUser.updateProfile({ displayName: name, photoURL: 'assets/imgs/avatar.png' })
          .then(() => {
            this.singupSuccess(loading, "password");
          })

      })
      .catch(err => {
        this.signupError(err, loading, "password");
      });

  }

  public signIn(provider: string, user?) {

    let loading = this.helper.createLoading('Signing you in...', 'bubbles');
    loading.present();

    switch (provider) {
      case "password":
        this.login(user.email, user.password)
          .then(() => {
            this.singinSuccess(loading, provider);
          })
          .catch(err => {
            this.signinError(err, loading, provider);
          });
        break;
      case "facebook":
        this.loginWithFacebook()
          .then(() => {
            this.singinSuccess(loading, provider);
          }).catch(err => {
            this.signinError(err, loading, provider);
          });
        break;
      case "twitter":
        this.loginWithTwitter()
          .then(() => {
            this.singinSuccess(loading, provider);
          })
          .catch(err => {
            this.signinError(err, loading, provider);
          });
        break;
      case "google":
        this.loginWithGoogle()
          .then(() => {
            this.singinSuccess(loading, provider);
          })
          .catch(err => {
            this.signinError(err, loading, provider);
          });
        break;
      default:
        console.log("default");
    }
  }

  // Sign in providers

  private login(email: string, password: string): Promise<any> {
    if (this.helper.doesNotHaveInternetConnection()) {
      return;
    }
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);

  }

  private loginWithGoogle() {
    if (this.helper.doesNotHaveInternetConnection()) {
      return;
    }
    return this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider())

  }
  private loginWithTwitter() {
    if (this.helper.doesNotHaveInternetConnection()) {
      return;
    }
    return this.afAuth.auth.signInWithRedirect(new firebase.auth.TwitterAuthProvider())

  }

  // private anonymousLogin() {
  //   if (this.helper.doesNotHaveInternetConnection()) {
  //     return;
  //   }
  //   return this.afAuth.auth.signInAnonymously()
  //     .then((user) => {
  //       this.singletonData.loggedIn = false;
  //     })
  //     .catch(err => {
  //       this.logger.logError(err);
  //     });
  // }

  private loginWithFacebook() {
    if (this.helper.doesNotHaveInternetConnection()) {
      return;
    }
    return this.fb.login(['email', 'public_profile']).then(res => {
      const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      return firebase.auth().signInWithCredential(facebookCredential);
    })
  }

  // Sign in helpers 

  private signinError(err: any, loading: any, provider_id: string) {

    FirebasePlugin.logEvent("sing_in_error", { provider_id: provider_id, error: err });
    fabric.Answers.sendLogIn(provider_id, false, err);

    loading.dismiss();
    let errorMessage = "Something went wrong with your login. Please try again."

    if (err && err.message) {
      errorMessage = err.message;
    }
    this.singletonData.loggedIn = false;
    this.toastService.showError(errorMessage);
  }

  private singinSuccess(loading: any, provider_id: string) {

    FirebasePlugin.logEvent("sing_in_success", { provider_id: provider_id });
    fabric.Answers.sendLogIn(provider_id, true);

    loading.dismiss();
  }

  // Sign up helpers

  private signupError(err: any, loading: any, provider_id: string) {

    FirebasePlugin.logEvent("sing_up", { provider_id: provider_id, error: err });
    fabric.Answers.sendSignUp(provider_id, false, err);

    let errorMessage = "Something went wrong with your login. Please try again.";

    if (err && err.message) {
      errorMessage = err.message;
    }
    this.toastService.showError(errorMessage);
    loading.dismiss();
  }

  private singupSuccess(loading: any, provider_id: string) {

    FirebasePlugin.logEvent("sing_up", { provider_id: provider_id });
    fabric.Answers.sendLogIn(provider_id, true);

    this.toastService.showSuccess("Successful registration");

    loading.dismiss();
  }

  // Signed in users helpers 

  private setUserFirebase(user) {

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);

    this.setUserDB(user);
    if (!user.isAnonymous) {
      return userRef.set(user, { merge: true });
    }
  }

  private setUserDB(user: CustomUser) {

    this.helper.updateUpdatesNotif(user.updatesNotif);
    this.helper.updateNewsNotif(user.newsNotif);
    this.helper.updateAdvertsNotif(user.advertsNotif);

    this.dataService.addOrUpdate('user', user);
    this.dataModel.user = this.userData;

    this.singletonData.loggedIn = true;
  }
}
