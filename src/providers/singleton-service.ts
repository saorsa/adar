
import { Injectable } from '@angular/core';

@Injectable()
export class SingletonServiceProvider {

  private _showLoading: boolean;
  private _loadingText: string;
  public _loggedIn: boolean;
  private _errorText: string;
  private _showError: boolean;
  private _successText: string;
  private _showSuccess: boolean;
  private _syncCollection: string;
  private _collectionToken: string;
  private _sideMenu: string;
  private _firebaseToken: string;

  constructor() {
  }

  public get showLoading(): boolean {
    return this._showLoading;
  }

  public set showLoading(value: boolean) {
    this._showLoading = value;
  }

  public get loadingText(): string {
    return this._loadingText;
  }

  public set loadingText(value: string) {
    this._loadingText = value;
  }

  public get loggedIn(): boolean {
    return this._loggedIn;
  }

  public set loggedIn(value: boolean) {
    this._loggedIn = value;
  }
  public get errorText(): string {
    return this._errorText;
  }

  public set errorText(value: string) {
    this._errorText = value;
  }

  public get showError(): boolean {
    return this._showError;
  }

  public set showError(value: boolean) {
    this._showError = value;
  }
  public get successText(): string {
    return this._successText;
  }

  public set successText(value: string) {
    this._successText = value;
  }

  public get showSuccess(): boolean {
    return this._showSuccess;
  }

  public set showSuccess(value: boolean) {
    this._showSuccess = value;
  }
  public get syncCollection(): string {
    return this._syncCollection;
  }

  public set syncCollection(value: string) {
    this._syncCollection = value;
  }

  public get collectionToken(): string {
    return this._collectionToken;
  }

  public set collectionToken(value: string) {
    this._collectionToken = value;
  }

  public get sideMenu(): string {
    return this._sideMenu;
  }

  public set sideMenu(value: string) {
    this._sideMenu = value;
  }
  public get firebaseToken(): string {
    return this._firebaseToken;
  }

  public set firebaseToken(value: string) {
    this._firebaseToken = value;
  }

  
}
