import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataServiceProvider } from './data-service';

@Injectable()
export class HistoryServiceProvider {

  constructor(
    public http: HttpClient,
    private dataService: DataServiceProvider
  ) { }

  add(item): Promise<any> {
    return this.dataService.get('history')
      .then(historyDoc => {
        let history = historyDoc.history;

        // Check if exist and remove
        let index = history.findIndex(i => i.name === item.name);
        if (index > -1) {
          history.splice(index, 1);
        }

        // push the item
        history.push(item);

        return this.dataService.updateDoc(historyDoc._id, { history });
      });
  }

  get(): Promise<any[]> {
    return this.dataService.get('history')
      .then(historyDoc => historyDoc.history);
  }

  initializeHistory(): Promise<void> {
    let history = {
      _id: "history",
      name: "History",
      history: []
    };

    return this.dataService.addDoc(history);
  }
}
