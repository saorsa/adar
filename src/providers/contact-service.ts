

import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Feedback } from '../models/feedback.model'

@Injectable()
export class ContactServiceProvider {

  private feedback: Observable<Feedback[]>;
  private feedbackCol: AngularFirestoreCollection<Feedback>;
  constructor(
    private afs: AngularFirestore,
  ) {
    this.feedbackCol = this.afs.collection('feedback');
    this.feedback = this.feedbackCol.valueChanges();
  }

  //TO DO - Feedback history

  sendFeedback(feedback: Feedback) {
    return this.feedbackCol.add(feedback);
  }



}
