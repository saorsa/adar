import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import 'rxjs/add/operator/map';

import PouchDB from "pouchdb";

import { DataModel } from "../models/data";
import { Logger } from "../providers/utils/logger";

@Injectable()
export class DataServiceProvider {

  private dataModel: DataModel;

  public _db;
  public _items;

  constructor(public events: Events, private logger: Logger) {
    this.dataModel = new DataModel();
  }

  get data() {
    return this.dataModel;
  }

  initDB() {
    this._db = new PouchDB("websql://adarDB", { adapter: 'websql' });
  }

  getProperty(property): Promise<any> {
    return this.get(property)
      .then(result => {
        if (result)
          return result.title
        else
          return result
      })
      .catch(err => {
        console.log("0")
        this.logger.logError(err);
      });
  }

  setProperty(property, value): Promise<any> {
    this.dataModel[property] = value;

    return this.addOrUpdate(property, value)
      .then(result => result)
      .catch(err => {
        console.log("1")
        this.logger.logError(err);
      });
  }

  addOrUpdate(property, value): Promise<any> {
    return this._db.get(property)
      .then((doc) => {
        if (doc.title != value) {
          return this.update(property, value, doc._rev);
        }
      }).catch(err => {
        return this.add(property, value);
      });
  }

  destroyDB(): Promise<void> {
    return this._db.destroy()
      .then(() => {
        this.logger.logError("Database destroyed successfully");
      })
      .catch(err => {
        console.log("2")
        this.logger.logError(err);
      });
  }

  private add(property, value): Promise<void> {
    return this._db.put({
      _id: property,
      title: value,
      timestamp: new Date()
    });
  }

  private update(property, value, rev): Promise<void> {

    let myObj = {
      _id: property,
      _rev: rev,
      title: value,
      timestamp: new Date()
    }

    return this._db.put(myObj)
      .then(response => response)
      .catch(err => {
        console.log("3")
        this.logger.logError(err);
      });
  }

  get(itemID): Promise<any> {
    return this._db.get(itemID)
      .then(response => response)
      .catch(err => {
        //Cannot find the object in the DB
        // console.log(err)
      });
  }

  getAll(): Promise<any> {
    if (!this._items) {
      return this._db.allDocs({ include_docs: true })
        .then(docs => {

          this._items = docs.rows.map(row => {
            row.doc.timestamp = new Date(row.doc.timestamp);
            return row.doc;
          });

          // Listen for changes on the database.
          this._db.changes({ live: true, since: 'now', include_docs: true })
            .on('change', this.onDatabaseChange);

          return this._items;
        });
    } else {
      // Return cached data as a promise
      return Promise.resolve(this._items);
    }
  }

  private onDatabaseChange = (change) => {
    var index = this.findIndex(this._items, change.id);
    var item = this._items[index];

    if (change.deleted) {
      if (item) {
        this.logger.logError('Deleted: ' + item);
      }
    } else {
      change.doc.Date = new Date(change.doc.Date);

      if (item && item._id === change.id) {
        this._items[index] = change.doc; // update

        if (item._id === 'recognized') {
          console.log("update")
          console.log(change.doc);
          this.events.publish('recognition:updated', change.doc);
        }
        else if (item._id === 'error') {
          this.events.publish('error:scan', change.doc);
        }
      } else {
        this._items.splice(index, 0, change.doc) // insert

        if (change.id === 'recognized') {
          console.log("insert")
          console.log('recognition:updated', change.doc);
          this.events.publish('recognition:updated', change.doc);
        }
        else if (item._id === 'error') {
          this.events.publish('error:scan', change.doc);
        }
      }
    }
  }

  // Docs

  addDoc(doc: object): Promise<any> {
    return this._db.put(doc);
  }

  updateDoc(id, newValues): Promise<any> {
    return this._db.get(id)
      .then(doc => {
        for (let prop in newValues) {
          doc[prop] = newValues[prop];
        }

        return this._db.put(doc);
      });
  }

  // Binary search, the array is by default sorted by _id.
  private findIndex(array, id) {
    var low = 0, high = array.length, mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  }
}
