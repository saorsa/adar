import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { Item } from '../models/craftar/item';
import { DataServiceProvider } from "../providers/data-service";
// import { HistoryServiceProvider } from "../providers/history-service";

import { Helper } from "../providers/utils/helper";
import { Logger } from "../providers/utils/logger";

import { SingletonServiceProvider } from '../providers/singleton-service';

@Injectable()
export class RecognitionProvider {

  private loadingSetCollection: string;
  private loadingGetCollection: string;
  private loadingAddCollection: string;
  private loadingSyncCollection: string;

  private hideAddLoading: boolean = true;
  private hideSetLoading: boolean = true;
  private resultFound: boolean = false;

  private readonly collectionToken: string = "68d26f46e6154af1";

  constructor(
    private dataService: DataServiceProvider,
    // private historyService: HistoryServiceProvider,
    private helper: Helper,
    private logger: Logger,
    private translateService: TranslateService,
    private singletonData: SingletonServiceProvider
  ) {

  }

  public startCraftAR(): void {
    let self = this;

    console.log("startCraftAR");

    self.translateService.get('LOADINGS.loadingSetCollection').subscribe((res: string) => {
      self.loadingSetCollection = res;
    });

    self.translateService.get('LOADINGS.loadingGetCollection').subscribe((res: string) => {
      self.loadingGetCollection = res;
    });

    self.translateService.get('LOADINGS.loadingAddCollection').subscribe((res: string) => {
      self.loadingAddCollection = res;
    });

    self.translateService.get('LOADINGS.loadingSyncCollection').subscribe((res: string) => {
      self.loadingSyncCollection = res;
    });

    this.singletonData.showLoading = true;
    this.singletonData.loadingText = "Loading ...";


    CraftARSDK.onStartCapture(() => self.onStartCapture());
    CraftARSDK.startCapture();

  }

  stopCraftAR(): void {
    console.log("stopCraftAR");
    CraftARSDK.stopFinder();
    CraftARSDK.closeView();
  }

  private setCollection(collection): void {
    let self = this;
    console.log("setCollection");
    this.singletonData.loadingText = self.loadingSetCollection;

    CraftARSDK.searchController = CraftAROnDeviceIR.searchController;
    CraftAROnDeviceIR.setCollection(collection, () => {
      CraftAROnDeviceIR.onSearchResults(results => {
        console.log("CraftAROnDeviceIR.onSearchResults");
        self.onSearchResults(results);
      });
      CraftAROnDeviceIR.onSearchError(error => {
        //self.onError(error);
        console.log("CraftAROnDeviceIR.onSearchError");
      });
      CraftARSDK.startFinder();
    }, error => {
      self.onError(error);
    }, progress => {
      if (parseInt(progress) === 100 && self.hideSetLoading) {
        //self.loadingSetCollection.dismiss();
        self.singletonData.showLoading = false;
        self.hideSetLoading = false;
      }
    });
  }

  public onStartCapture(): void {
    let self = this;
    console.log("onStartCapture");
    this.singletonData.loadingText = self.loadingGetCollection;

    CraftARCollectionManager.getCollectionWithToken(this.collectionToken, collection => {

      // if (JSON.parse(syncCollection)) {
      this.singletonData.loadingText = self.loadingSyncCollection;
      CraftARCollectionManager.syncCollection(collection, syncdCollection => {

        self.setCollection(syncdCollection);
      }, error => {
        self.onError(error);

      });
      // }
      // else {
      //   self.setCollection(collection);
      // }
    }, error => {
      this.singletonData.loadingText = self.loadingAddCollection;
      CraftARCollectionManager.addCollectionWithToken(this.collectionToken, collection => {
        self.setCollection(collection);
      }, error => {
        self.onError(error);
      }, progress => {
        if (parseInt(progress) === 100 && self.hideAddLoading) {
          self.hideAddLoading = false;
        }
      });
    })
  }

  private onSearchResults(results) {
    let self = this;
    console.log("startCraftAR");
    if (results.length > 0) {
      let item = results[0].item as Item;
      if (item.isAR) {

        // FirebasePlugin.logEvent("augmented_reality", { item_id: item.name });
        // fabric.Answers.sendCustomEvent("augmented_reality", { item_id: item.name });

        CraftARTracking.addItem(item, () => {
          CraftARTracking.startTracking();
        }, (err) => {
          console.log(err);
          CraftARSDK.startFinder();
        });
      } else {
        //   // Add to history
        //   let image = results[0].matchedImage.thumbnail320Url;
        //   self.historyService.add({
        //     name: item.name,
        //     url: item.url,
        //     desc: item.custom,
        //     img: image
        //   });

        self.getResults(item);
      }
    }
  }

  private getResults(item: Item): void {
   
    var result = item.url;
    if (result.length && this.resultFound == false) {
      this.resultFound = true;

      // FirebasePlugin.logEvent("image_recognition", { item_id: item.name });
      // fabric.Answers.sendCustomEvent("image_recognition", { item_id: item.name });

      var recognized = result + ' ' + this.helper.getRandomString(10);
      console.log(recognized);
      this.dataService.addOrUpdate('recognized', recognized);

      CraftARSDK.closeView();
    }
  }

  onError(error: any) {
    console.log("onError");
    this.dataService.addOrUpdate('error', error);
    this.logger.logError(error);
    this.stopCraftAR();
  }
}
