import { DataModel } from './../../models/data';
import { DataServiceProvider } from './../data-service';
import { Injectable } from '@angular/core';
import { AlertButton, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';

import { AndroidPermissions } from '@ionic-native/android-permissions';

import { Logger } from './logger';

@Injectable()
export class Helper {

  private possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private logger: Logger,
    private androidPermissions: AndroidPermissions,
    private dataService: DataServiceProvider,
    private dataModel: DataModel
  ) { }

  showAlert(title: string, subTitle: string, buttons: (string | AlertButton)[]): void {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: buttons
    });

    alert.present();
  }

  createLoading(message: string, spinner: string): Loading {
    try {
      let loading = this.loadingCtrl.create({
        content: message,
        spinner: spinner
      });

      return loading;
    }
    catch (e) {
      this.logger.logError(e);
    }
  }

  showLoading(title: string, duration): Promise<void> {
    return new Promise((resolve, reject) => {
      let loading = this.loadingCtrl.create({
        content: title
      });

      loading.present();

      setTimeout(() => {
        loading.dismiss();
        resolve();
      }, duration);
    });
  }

  showToast(title: string, duration): Promise<void> {
    return new Promise((resolve, reject) => {
      let toast = this.toastCtrl.create({
        message: title,
        duration: duration,
        position: 'bottom'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
      resolve();
    });
  }

  getRandomInt(min, max): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getRandomString(length: number): string {
    let str = '';

    for (let i = 0; i < length; i += 1) {
      str += this.possibleChars.charAt(this.getRandomInt(0, this.possibleChars.length));
    }

    return str;
  }

  doesNotHaveInternetConnection(): boolean {
    var networkState = navigator['connection'].type;
    if (networkState === Connection.UNKNOWN ||
      networkState === Connection.NONE) return true;

    return false;
  }

  checkPermission(permission): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.androidPermissions.checkPermission(permission).then(
        result => resolve(result.hasPermission),
        err => this.androidPermissions.requestPermission(permission)
      );
    });
  }

  askPermissions(): Promise<boolean> {
    return new Promise((resolve, reject) => {

      var self = this;
      var permissions = [this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE];

      self.checkPermission(this.androidPermissions.PERMISSION.CAMERA)
        .then(result => {
          if (result) {
            return resolve();
          }
          else {
            self.androidPermissions.requestPermissions(permissions)
              .then(result => {
                return resolve()
              }
              ).catch(err => {
                this.logger.logError(err);
                return reject()
              });
          }
        }).catch(err => {
          this.logger.logError(err);
          return reject();
        });

    });
  }


  formatDataFromProvider(user, fbToken, onlineUser) {

    let photoURL = "assets/imgs/avatar.png"
    let displayName = "John Doe";
    let email = "example@adar.group";
    let uid = "1234567890qwertyuiop";

    if (onlineUser) {

      displayName = onlineUser.displayName ? onlineUser.displayName : "";
      email = onlineUser.email ? onlineUser.email : "";
      uid = onlineUser.uid;

      if (onlineUser.providerData.length > 0) {
        //Checks user`s provider
        if (onlineUser.photoURL) photoURL = onlineUser.photoURL;
        if (onlineUser.providerData[0].email) email = onlineUser.providerData[0].email;

        switch (onlineUser.providerData[0].providerId) {
          case "twitter.com":

            if (photoURL.indexOf("_normal") > -1) {
              photoURL = onlineUser.photoURL.replace('_normal', '_400x400');
            }

            break;
          case "facebook.com":

            if (photoURL.indexOf("?width=400&height=400") == -1) {
              photoURL = onlineUser.photoURL + '/?width=400&height=400';
            }
            break;
          case "google.com":

            if (photoURL.indexOf("?sz=400") == -1) {
              photoURL = onlineUser.photoURL + '?sz=400';
            }

            break;
        }
      }
      return this.formatUserData(onlineUser, displayName, email, photoURL, fbToken)
    }
    else {

      displayName = user.displayName ? user.displayName : "";
      email = user.email ? user.email : "";
      uid = user.uid;

      if (user.providerData.length > 0) {
        //Checks user`s provider
        if (user.photoURL) photoURL = user.photoURL;
        if (user.providerData[0].email) email = user.providerData[0].email;

        switch (user.providerData[0].providerId) {
          case "twitter.com":

            if (photoURL.indexOf("_normal") > -1) {
              photoURL = user.photoURL.replace('_normal', '_400x400');
            }

            break;
          case "facebook.com":

            if (photoURL.indexOf("?width=400&height=400") == -1) {
              photoURL = user.photoURL + '/?width=400&height=400';
            }
            break;
          case "google.com":

            if (photoURL.indexOf("?sz=400") == -1) {
              photoURL = user.photoURL + '?sz=400';
            }

            break;
        }
      }
      return this.formatUserData(user, displayName, email, photoURL, fbToken)
    }
  }

  private formatUserData(user, displayName, email, photoURL, fbToken) {

    return {
      uid: user.uid,
      email: email,
      displayName: displayName,
      photoURL: photoURL,
      showIntro: user.showIntro == null ? true : user.showIntro,
      language: user.language ? user.language : 'en',
      providerData: user.providerData ? user.providerData : {},
      firebaseToken: fbToken,
      isAnonymous: user.isAnonymous,
      updatesNotif: user.updatesNotif == null ? true : user.updatesNotif,
      newsNotif: user.newsNotif == null ? true : user.newsNotif,
      advertsNotif: user.advertsNotif == null ? true : user.advertsNotif
    }
  }

  public getUserFromDB(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.dataService.getProperty('user')
        .then(userDB => {
          resolve(userDB);
        })
        .catch(err => {
          reject('No such user in the DB');
        })
    });
  }

  updateUpdatesNotif(checked) {
    this.dataModel.changeUpdatesNotif(checked);
    this.dataService.addOrUpdate('updatesNotif', checked)
      .catch(err => {
        this.logger.logError(err);
      });
  }

  updateNewsNotif(checked) {
    this.dataModel.changeNewsNotif(checked);
    this.dataService.addOrUpdate('newsNotif', checked)
      .catch(err => {
        this.logger.logError(err);
      });
  }

  updateAdvertsNotif(checked) {
    this.dataModel.changeAdvertsNotif(checked);
    this.dataService.addOrUpdate('advertsNotif', checked)
      .catch(err => {
        this.logger.logError(err);
      });
  }
  updateGeneralNotif(checked) {
    this.dataService.addOrUpdate('generalNotif', checked)
      .catch(err => {
        this.logger.logError(err);
      });
  }


}