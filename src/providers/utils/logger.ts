import { Injectable } from '@angular/core';

@Injectable()
export class Logger {

  constructor() { }

  logError(err) {
    FirebasePlugin.logEvent("code_error", { error: err });
    fabric.Answers.sendCustomEvent("code_error", err);
    console.log(err);
  }
}
