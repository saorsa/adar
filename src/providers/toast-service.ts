import { SingletonServiceProvider } from './singleton-service';

import { Injectable } from '@angular/core';
import { timer } from 'rxjs/observable/timer';

@Injectable()
export class ToastServiceProvider {

  constructor(
    public singletonData: SingletonServiceProvider
  ) {

  }

  showError(errorMessage: string) {
    this.singletonData.showError = true;
    this.singletonData.showSuccess = false;
    this.singletonData.errorText = errorMessage;
    this.toastTimer();
  }

  showSuccess(successMessage: string) {
    this.singletonData.showError = false;
    this.singletonData.showSuccess = true;
    this.singletonData.successText = successMessage;
    this.toastTimer();
  }

  hideToast() {

    this.singletonData.errorText = "";
    this.singletonData.successText = "";

    if (this.singletonData.showError) {
      this.singletonData.showError = false;
    }
    if (this.singletonData.showSuccess) {
      this.singletonData.showSuccess = false;
    }
  }

  private toastTimer() {
    timer(5000).subscribe(() => {
      this.singletonData.showSuccess = false;
      this.singletonData.showError = false;
      this.singletonData.errorText = 'Error';
      this.singletonData.successText = 'Success';
    })
  }
}
