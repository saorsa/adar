import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class HttpServiceProvider {

  constructor(public http: HttpClient) { }

  get(endpoint: string): Promise<any> {
    return this.http.get(endpoint).toPromise();
  }

  public post(endpoint: string, data: any, options: any): Promise<any> {
    return this.http.post(endpoint, data, options).toPromise();
  }

  public put(endpoint: string, data: any, options: any): Promise<any> {
    return this.http.put(endpoint, data, options).toPromise();
  }
}
