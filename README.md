# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Android build ###

ionic cordova build android --prod --release

"C:\Program Files\Java\jdk1.8.0_161\bin\keytool" -genkey -v -keystore adar.keystore -alias com.app.adar.group -keyalg RSA -keysize 2048 -validity 10000

"C:\Program Files\Java\jdk1.8.0_161\bin\jarsigner" -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore adar.keystore "D:\Projects\Git\AdAR_Group\adar\platforms\android\build\outputs\apk\release\android-release-unsigned.apk" com.app.adar.group

"C:\Users\deyan\AppData\Local\Android\sdk\build-tools\27.0.3\zipalign" -v 4 "D:\Projects\Git\AdAR_Group\adar\platforms\android\build\outputs\apk\release\android-release-unsigned.apk" AdAR_2.0.0.apk

adar.keystore key: @adarDev1


### Good to know ###

- Adding fabric plugin - TO Fill

cordova plugin add cordova-fabric-plugin --variable FABRIC_API_KEY=199a237bc4adfbdc1ee541efad8143c336cd1fb9 --variable FABRIC_API_SECRET=3e6d3fbbaa37a92fd7a194bc05586c0f65babcb701d7a67443623ecd291b1ee9

- Users Credentilas - To Fill

### TO DOs ###

Version 2.0

- Cooler splashscreen - Done
-- Initial loading - Done
-- Recognition loading - Done
- New design beta - Done
- Restructure the application - Done
- Auth - Done
-- Login - Done
--- Twitter - Done
--- Google - Done
--- Facebook - Done
--- Add email/password login - Done
--- Redirect after login - Done
--- Redirect after logout - Done
- Registration - Done
- Add custom Users table in Firebase DB - Done
-- Settings per user - Done
-- Contacts from user - Done
-- Profile page - Done
--- Name - Done
--- Email - Done
--- Picture (from provider) - Done
--- Phone number (to have) - Postponed
- Add input validations - Done
-- Registration Form - Done
-- Login Form - Done
-- Contact Form - Done
- Custom Error and Success Toasts - Done
-- Some cool way to show that the user is offline (toast below the top of the page) - Done
-- Add internet connection check - Done
- Contact Form - Done
- Crashalitcs from Fabric - Done
- Extend ErrorHandler - Done
- Test app rate store - Done
- Save Firebase Token in the FireStore DB - Done
- Settings for anonymous users - phone settings - Done
- Add Remote Config - Done

Version 2.1

- New design final 
- Secure the User talbe in the DB
- Facebook sign in with redirect - Imposible
- Request password reset
- Hide the Error and Success toasts after some time - 3 ~ 5 seconds

Version x.x

- Remove logic from (Helper and Logger) form pages.ts    
- Move label initialization in app.components
- Familirize yourself with RemoteConfig 
- Backend integration
- Share something cool on social networks
- Add History per user
- Try to integrate Fabric.io - Crashalytics for Ionic
- Log every app expection is DB

General 

- Unit Testing - 
- Fill README with current version capabilities 
- Check if we can load the CraftAR collection on app start instead of on recognition start
- Document Cordova "hack"
- Check Georgis` solution - will it be of any help
- Extend the README file
- Checkout  Progressive Web App - Ionic guide
- Check Facebook analytics and compare with Firebase
- Check what can we do with service-worker.js
- Familirize yourself with FirebasePlugin.addEvent 
- Check this out - https://www.npmjs.com/package/ionic-ng-walkthrough
- Check this out - https://market.ionicframework.com/plugins/ionic-2-ionic-walkthrough
- Check this out - https://introjs.com/
- Custom Error handlers
